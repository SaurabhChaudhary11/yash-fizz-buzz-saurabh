import React,{Component} from 'react';
import CheckNumber from './CheckNumber';

import './App.css';

class App extends React.Component {

constructor(props){
  super(props);
  
  this.state ={ 
    numbers:[]
  };

  this.state={
    activePage: 1
  };

  this.onCheckNumber = this.onCheckNumber.bind(this);
}

handlePageChange(pageNumber) {
  console.log(`active page is ${pageNumber}`);
  this.setState({activePage: pageNumber});
}

onCheckNumber(num){
 
  var rows = [];
  for (var i = 1; i <=num; i++) {
  
     if(i%3===0)
    {
      if(i%5===0)
      rows.push("Fizz Buzz");
      else
      rows.push(<h8 className="fizz">"Fizz"</h8>);
    }
    else if(i%5===0)
    rows.push(<h8 className="buzz">"Buzz"</h8>);
    else
    rows.push(i);
}

this.setState({
  numbers: Object.assign(this.state.numbers, rows)
});


}

  render() {
     return (
        <div className="App">
          <h1>Fizz Buzz</h1>
           <CheckNumber
            onCheckNumber = {this.onCheckNumber}
            />
<div>
        <Pagination
          activePage={this.state.activePage}
          itemsCountPerPage={10}
          totalItemsCount={450}
          pageRangeDisplayed={5}
          onChange={::this.handlePageChange}
        />
      </div>
          {
          this.state.numbers.map(number=>(
          <tr><td>{number}</td></tr>
        )
        )
    
        }
          
      </div>
     );
  }
}
export default App;
